**Feladat leírása:**

1. Készítsetek egy felületet: legyen rajta egy JList és egy JButton(Betölt) - a Jlist-ben a Betölt gomb megnyomására töltódjön be az adatbázisból a Kategoriak tábla tartalma.

2. Készítsetek további gombokat: Hozzáadás, Módosítás - A hozzádás gomb tudjon felvenni új kategóriákat, a módosítás gomb módosítson egy meglevő kategóriát.
Ezek a változtatások kerüljenek be az adatbázisba is.

**Insert**
```java
pst = (PreparedStatement) conn.prepareStatement("insert into kategoriak (kategoria_nev) values (?)",
					Statement.RETURN_GENERATED_KEYS);
			conn.setAutoCommit(false);
			pst.setString(1, kategoriaNev);
			pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			int id = 0;
			if (rs.next()) {
				id = rs.getInt(1);
			}
			System.out.println("KAteegoria felveve a kovetkezo id-val: " + id);
			conn.commit();
```
**Törlés**
```java
String sql = "DELETE FROM kategoriak WHERE kategoria_nev = ?";
			pst = (PreparedStatement) conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			pst.setString(1, kategoriaNeve);
			pst.executeUpdate();
			System.out.println("Kategoria törlése: " + kategoriaNeve);
			conn.commit();
```

**Módosítás**
```java
String sql = "UPDATE kategoriak SET kategoria_nev = ? WHERE id = ?";
			pst = (PreparedStatement) conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			pst.setString(1, kategoriaNev);
			pst.setInt(2, id);
			pst.executeUpdate();
			conn.commit();
```

![picture](EER Diagram.PNG)
