package model;

import java.sql.Date;

public class Szamla {

	private int id;
	private Date kelt;
	private Double vegosszeg;
	private String fizeto_eszkoz;
	private int vasarlasId;

	public Szamla(Date kelt, Double vegosszeg, String fizeto_eszkoz, int vasarlasId) {
		this.kelt = kelt;
		this.vegosszeg = vegosszeg;
		this.fizeto_eszkoz = fizeto_eszkoz;
		this.vasarlasId = vasarlasId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getKelt() {
		return kelt;
	}

	public void setKelt(Date kelt) {
		this.kelt = kelt;
	}

	public Double getVegosszeg() {
		return vegosszeg;
	}

	public void setVegosszeg(Double vegosszeg) {
		this.vegosszeg = vegosszeg;
	}

	public String getFizeto_eszkoz() {
		return fizeto_eszkoz;
	}

	public void setFizeto_eszkoz(String fizeto_eszkoz) {
		this.fizeto_eszkoz = fizeto_eszkoz;
	}

	public int getVasarlasId() {
		return vasarlasId;
	}

	public void setVasarlasId(int vasarlasId) {
		this.vasarlasId = vasarlasId;
	}

}
