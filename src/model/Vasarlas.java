package model;

import java.sql.Date;

public class Vasarlas {

	private int id;
	private int vasarloId;
	private Date idopont;

	public Vasarlas(int vasarloId, Date idopont) {
		this.vasarloId = vasarloId;
		this.idopont = idopont;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVasarloId() {
		return vasarloId;
	}

	public void setVasarloId(int vasarloId) {
		this.vasarloId = vasarloId;
	}

	public Date getIdopont() {
		return idopont;
	}

	public void setIdopont(Date idopont) {
		this.idopont = idopont;
	}

}
