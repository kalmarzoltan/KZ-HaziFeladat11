package model;

public class Kategoria {

	private int id;
	private String kategoriaNev;

	public Kategoria(String kategoriaNev) {
		this.kategoriaNev = kategoriaNev;
	}

	public Kategoria(int id, String kategoriaNev) {
		this.id = id;
		this.kategoriaNev = kategoriaNev;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKategoriaNev() {
		return kategoriaNev;
	}

	public void setKategoriaNev(String kategoriaNev) {
		this.kategoriaNev = kategoriaNev;
	}

	@Override
	public String toString() {
		return kategoriaNev;
	}

}
