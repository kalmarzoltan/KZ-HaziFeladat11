package model;

public class Vasarlo {

	private int id;
	private String vezetekNev;
	private String keresztNev;
	private String kozepsoNev;
	private String nem;

	public Vasarlo(int id, String vezetekNev, String keresztNev, String kozepsoNev, String nem) {
		this.id = id;
		this.vezetekNev = vezetekNev;
		this.keresztNev = keresztNev;
		this.kozepsoNev = kozepsoNev;
		this.nem = nem;
	}

	public Vasarlo(int id, String vezetekNev, String keresztNev, String nem) {
		this.id = id;
		this.vezetekNev = vezetekNev;
		this.keresztNev = keresztNev;
		this.nem = nem;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVezetekNev() {
		return vezetekNev;
	}

	public void setVezetekNev(String vezetekNev) {
		this.vezetekNev = vezetekNev;
	}

	public String getKeresztNev() {
		return keresztNev;
	}

	public void setKeresztNev(String keresztNev) {
		this.keresztNev = keresztNev;
	}

	public String getKozepsoNev() {
		return kozepsoNev;
	}

	public void setKozepsoNev(String kozepsoNev) {
		this.kozepsoNev = kozepsoNev;
	}

	public String getNem() {
		return nem;
	}

	public void setNem(String nem) {
		this.nem = nem;
	}

}
