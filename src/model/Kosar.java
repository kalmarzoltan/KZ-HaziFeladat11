package model;

public class Kosar {

	private int id;
	private int arucikkId;
	private double mennyiseg;
	private int vasarlasId;

	public Kosar(int arucikkId, double mennyiseg, int vasarlasId) {
		this.arucikkId = arucikkId;
		this.mennyiseg = mennyiseg;
		this.vasarlasId = vasarlasId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArucikkId() {
		return arucikkId;
	}

	public void setArucikkId(int arucikkId) {
		this.arucikkId = arucikkId;
	}

	public double getMennyiseg() {
		return mennyiseg;
	}

	public void setMennyiseg(double mennyiseg) {
		this.mennyiseg = mennyiseg;
	}

	public int getVasarlasId() {
		return vasarlasId;
	}

	public void setVasarlasId(int vasarlasId) {
		this.vasarlasId = vasarlasId;
	}

}
