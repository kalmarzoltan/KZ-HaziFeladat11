package model;

public class Arucikk {

	private int id;
	private String termekNev;
	private Double ar;
	private String mennyisegiEgyseg;
	private int kategoriaId;

	public Arucikk(String termekNev, Double ar, String mennyisegiEgyseg, int kategoriaId) {
		this.termekNev = termekNev;
		this.ar = ar;
		this.mennyisegiEgyseg = mennyisegiEgyseg;
		this.kategoriaId = kategoriaId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTermekNev() {
		return termekNev;
	}

	public void setTermekNev(String termekNev) {
		this.termekNev = termekNev;
	}

	public Double getAr() {
		return ar;
	}

	public void setAr(Double ar) {
		this.ar = ar;
	}

	public String getMennyisegiEgyseg() {
		return mennyisegiEgyseg;
	}

	public void setMennyisegiEgyseg(String mennyisegiEgyseg) {
		this.mennyisegiEgyseg = mennyisegiEgyseg;
	}

	public int getKategoriaId() {
		return kategoriaId;
	}

	public void setKategoriaId(int kategoriaId) {
		this.kategoriaId = kategoriaId;
	}

}
