use aruhaz;

insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('Paradicsom', 600, 'kg', 1);
insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('Paprika', 500, 'kg', 1);
insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('Kifli', 25, 'db', 3);
insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('Zsemlye', 30, 'db', 3);
insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('Tükör', 2300, 'db', 4);
insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('WC Kefe', 1500, 'db', 4);
insert into arucikkek (termek_nev, ar, mennyisegi_egyseg, kategoria_id) values('Alma', 350, 'kg', 2);

commit;
