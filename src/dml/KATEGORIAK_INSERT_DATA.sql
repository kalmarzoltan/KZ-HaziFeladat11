use aruhaz;


-- dml
insert into kategoriak (kategoria_nev) values ('Zöldség');
insert into kategoriak (kategoria_nev) values ('Gyümölcs');
insert into kategoriak (kategoria_nev) values ('Pékárú');
insert into kategoriak (kategoria_nev) values ('Szaniter');
commit;

-- tippek trükkök
-- SET autocommit = 0;
-- insert into kategoriak (kategoria_nev) values ('Gyümölcs');
-- insert into kategoriak (kategoria_nev) values ('Pékárú');
-- insert into kategoriak (kategoria_nev) values ('Szaniter');
-- select * from kategoriak;
-- commit;
-- rollback;

-- select * from kategoriak;
-- truncate table kategoriak;
