package gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.DefaultListModel;

import model.Kategoria;

public class Vezerlo {

	private DefaultListModel defaultListModel;

	public Vezerlo(DefaultListModel<Kategoria> defaultListModel) {

		this.defaultListModel = new DefaultListModel<>();
	}

	public DefaultListModel<Kategoria> readKategoriaFromDB() {

		Connection conn = null;

		String url = "jdbc:mysql://localhost:3306/aruhaz";
		String user = "root";
		String password = "1234";
		DefaultListModel defaultListModel = new DefaultListModel();
		try {
			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Connected to database");
			Statement st = conn.createStatement();

			ResultSet rs = st.executeQuery("select k.ID, k.KATEGORIA_NEV from kategoriak k");

			Kategoria kategoria;

			while (rs.next()) {
				System.out.println(rs.getString(1) + "|" + rs.getString(2));
				kategoria = new Kategoria(rs.getInt("id"), rs.getString("kategoria_nev"));

				defaultListModel.addElement(kategoria);
			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage().toString());
		}

		return defaultListModel;
	}

	public DefaultListModel<Kategoria> getListModel() {
		return getListModel();
	}

	public void setListModel(DefaultListModel<Kategoria> listModel) {
		this.defaultListModel = listModel;
	}

}
