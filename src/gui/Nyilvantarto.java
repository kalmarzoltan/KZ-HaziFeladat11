package gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import main.KategoriaRemover;
import model.Kategoria;

public class Nyilvantarto extends JFrame {

	private JPanel contentPane;
	private JList<Kategoria> jList;
	private DefaultListModel<Kategoria> defListmodel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Nyilvantarto frame = new Nyilvantarto();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Nyilvantarto() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		defListmodel = new DefaultListModel<Kategoria>();

		jList = new JList<Kategoria>();
		jList.setBounds(30, 70, 150, 180);
		contentPane.add(jList);

		Vezerlo vezerlo = new Vezerlo(defListmodel);

		defListmodel = (DefaultListModel<Kategoria>) vezerlo.readKategoriaFromDB();
		jList.setModel(defListmodel);

		JLabel lblKategoriakListaja = new JLabel("Kategoriak listaja");
		lblKategoriakListaja.setBounds(30, 45, 150, 14);
		contentPane.add(lblKategoriakListaja);

		JLabel lblKategoriaNyilvntarto = new JLabel("Kategoria nyilvantarto");
		lblKategoriaNyilvntarto.setFont(new Font("Arial", Font.PLAIN, 14));
		lblKategoriaNyilvntarto.setHorizontalAlignment(SwingConstants.CENTER);
		lblKategoriaNyilvntarto.setBounds(30, 11, 394, 23);
		contentPane.add(lblKategoriaNyilvntarto);

		JButton btnHozzaad = new JButton("Hozzaadas");
		btnHozzaad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				KategoriaDialog ad = new KategoriaDialog(defListmodel, jList.getSelectedIndex());
				ad.setVisible(true);

			}
		});
		btnHozzaad.setBounds(190, 67, 89, 23);
		contentPane.add(btnHozzaad);

		JButton btnModosit = new JButton("Modositas");
		btnModosit.setBounds(190, 101, 89, 23);
		contentPane.add(btnModosit);
		btnModosit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				KategoriaDialog ad = new KategoriaDialog(defListmodel, jList.getSelectedIndex());
				ad.setVisible(true);

			}
		});

		JButton btnTorol = new JButton("Torles");
		btnTorol.setBounds(190, 135, 89, 23);
		contentPane.add(btnTorol);
		btnTorol.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// DB-ből törlés
				System.err.println(defListmodel.getElementAt(jList.getSelectedIndex()));
				String töröl = defListmodel.getElementAt(jList.getSelectedIndex()).getKategoriaNev();
				System.out.println(töröl);
				KategoriaRemover kategoriaRemover = new KategoriaRemover();
				kategoriaRemover.removeKategoria(töröl);
				// jListből kiszed
				System.out.println("Torles: " + jList.getSelectedIndex());
				defListmodel.removeElementAt(jList.getSelectedIndex());
			}
		});
	}
}
