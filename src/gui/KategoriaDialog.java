package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import main.KategoriaAdder;
import main.KategoriaModder;
import model.Kategoria;

public class KategoriaDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	// private JTextField tfId;
	private JTextField tfKategoriaNev;

	private DefaultListModel<Kategoria> defaultListModel;

	/**
	 * Create the dialog.
	 */
	public KategoriaDialog(DefaultListModel<Kategoria> listmodel, Integer selectedIndex) {

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		this.defaultListModel = listmodel;

		{
			JLabel lblAlkalmazottFelvtele = new JLabel("Kategoria felvetele/Módosítasa");
			lblAlkalmazottFelvtele.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lblAlkalmazottFelvtele.setHorizontalAlignment(SwingConstants.CENTER);
			lblAlkalmazottFelvtele.setBounds(10, 11, 414, 20);
			contentPanel.add(lblAlkalmazottFelvtele);
		}
		{
			// JLabel lblId = new JLabel("Id:");
			// lblId.setHorizontalAlignment(SwingConstants.RIGHT);
			// lblId.setBounds(58, 60, 125, 14);
			// contentPanel.add(lblId);
		}
		{
			JLabel kategoriaNev = new JLabel("Kategoria nev:");
			kategoriaNev.setHorizontalAlignment(SwingConstants.RIGHT);
			kategoriaNev.setBounds(68, 85, 115, 14);
			contentPanel.add(kategoriaNev);
		}

		// tfId = new JTextField();
		// tfId.setBounds(210, 57, 144, 20);
		// contentPanel.add(tfId);
		// tfId.setColumns(10);

		tfKategoriaNev = new JTextField();
		tfKategoriaNev.setColumns(10);
		tfKategoriaNev.setBounds(210, 82, 144, 20);
		contentPanel.add(tfKategoriaNev);

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {

						if (selectedIndex == -1) {

							Kategoria kategoria = new Kategoria(tfKategoriaNev.getText());

							listmodel.addElement(kategoria);

							// adatbázisba be -> kategoriaAdder
							KategoriaAdder kategoriaAdder = new KategoriaAdder();
							kategoriaAdder.addKategoria(kategoria.getKategoriaNev());

						} else {

							System.out.println("selected for modification: " + selectedIndex);
							// Nehogy valamit átírjunk amit nem látunk -> nincs new Kategoria

							Integer id = listmodel.get(selectedIndex).getId();

							// adatbázisba be -> kategoriaModder

							KategoriaModder kategoriaModder = new KategoriaModder();
							kategoriaModder.modKategoria(id, tfKategoriaNev.getText());
						}
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
