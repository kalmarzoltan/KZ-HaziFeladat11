package main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class KategoriaModder {

	public void modKategoria(int id, String ujNev) {

		System.out.println("Kategoria modositasa:");

		String kategoriaNev = ujNev;
		String url = "jdbc:mysql://localhost:3306/aruhaz";
		String user = "root";
		String password = "1234";

		DBHelper dBhelper = new DBHelper(url, user, password);
		Connection conn = dBhelper.getConn();

		try {

			System.out.println("Connected to database");
			// LÉNYEG
			PreparedStatement pst = null;

			ResultSet rs = null;

			kategoriakKiirasa(conn);

			String sql = "UPDATE kategoriak SET kategoria_nev = ? WHERE id = ?";
			pst = (PreparedStatement) conn.prepareStatement(sql);
			conn.setAutoCommit(false);
			pst.setString(1, kategoriaNev);
			pst.setInt(2, id);
			pst.executeUpdate();
			conn.commit();

			kategoriakKiirasa(conn);

		} catch (SQLException ex) {
			System.out.println(ex.getMessage().toString());
		}

	}

	public static void kategoriakKiirasa(Connection conn) throws SQLException {

		PreparedStatement pst = (PreparedStatement) conn.prepareStatement("select * from kategoriak");
		ResultSet rs = pst.executeQuery();
		while (rs.next()) {
			int id = rs.getInt("ID");
			String kategoriaNev = rs.getString("KATEGORIA_NEV");
			System.out.println(id + "-" + kategoriaNev);
		}
	}

}
