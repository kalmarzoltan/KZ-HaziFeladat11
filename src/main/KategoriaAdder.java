package main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.PreparedStatement;

public class KategoriaAdder {

	public void addKategoria(String kategoriaNev) {

		System.out.println("Kategoria felvetele:");

		String kategoria = kategoriaNev;
		String url = "jdbc:mysql://localhost:3306/aruhaz";
		String user = "root";
		String password = "1234";

		DBHelper dBhelper = new DBHelper(url, user, password);
		Connection conn = dBhelper.getConn();

		try {

			System.out.println("Connected to database");

			PreparedStatement pst = null;

			ResultSet rs = null;

			kategoriakKiirasa(conn);

			pst = (PreparedStatement) conn.prepareStatement("insert into kategoriak (kategoria_nev) values (?)",
					Statement.RETURN_GENERATED_KEYS);
			conn.setAutoCommit(false);
			pst.setString(1, kategoriaNev);

			pst.executeUpdate();

			rs = pst.getGeneratedKeys();
			int id = 0;
			if (rs.next()) {
				id = rs.getInt(1);
			}

			System.out.println("KAteegoria felveve a kovetkezo id-val: " + id);
			conn.commit();

			kategoriakKiirasa(conn);

		} catch (SQLException ex) {
			System.out.println(ex.getMessage().toString());
		}

	}

	public static void kategoriakKiirasa(Connection conn) throws SQLException {

		PreparedStatement pst = (PreparedStatement) conn.prepareStatement("select * from kategoriak");
		ResultSet rs = pst.executeQuery();
		while (rs.next()) {
			int id = rs.getInt("ID");
			String kategoriaNev = rs.getString("KATEGORIA_NEV");
			System.out.println(id + "-" + kategoriaNev);
		}
	}

}
